let facade = require('./facade')

function route(app)
{
	app.get('/login', (req, res) =>
	{
		let conn_error = req.query.conn_error
		res.render('login', {title: 'Connexion', conn_error: conn_error})
	})

	app.post('/login', (req, res) =>
	{
		let email = req.body.email
		let password = req.body.password
		facade.getUser(email, password, (error,rows) => {
			if(rows == '')
			{
				res.redirect('/login?conn_error=USER_NOT_FOUND');
			}
			else
			{
				let user = rows[0]
				res.cookie('user', user, { maxAge: 900000, httpOnly: true })
				res.redirect('/')
			}
		});
		
	})
}

module.exports =
{
	route: route
}