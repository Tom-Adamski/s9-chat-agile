let facade = require('./facade')

function route(app)
{
	app.get('/chat/:id_channel', (req, res) =>
	{
		if(req.cookies['user']){
			facade.getChannels((erreur, channels) =>
			{
				let idChannel = req.params.id_channel
				facade.getMessagesFromChannel(idChannel, (error, messages) =>
				{
					if(error){
						res.render('erreur', {title: 'Erreur', erreur: "Erreur lors du chargement des messages"})
					}
					else {
						res.render('chat', {title: 'Accueil', messages: messages, idChannel: idChannel, channels : channels})
					}
				})
			})
			
		}
		else {
			res.redirect('/login')
		}
	})
}

module.exports =
{
	route: route
}