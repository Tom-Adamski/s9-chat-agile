// MODULES ////////////////////////////////////////////////////////////////////

// Framework express et autres modules pour le developpement d'une application web
const express = require('express')
var cookieParser = require('cookie-parser')

// ROUTES /////////////////////////////////////////////////////////////////////

const routeIndex = require('./chat')
const routeChat = require('./index')
const routeEnvoyerMessage = require('./envoyer_message')
const routeLogin = require('./login')
const routeLogout = require('./logout')
const routeAdmin = require('./admin')
const routeChannels = require('./channels')

// LOGIQUE FICHIER ////////////////////////////////////////////////////////////

let app = express()
let port = 3000

app.set('view engine', 'pug')
app.set('views', './templates')
app.use(express.static('static'))
app.use(express.json())
app.use(express.urlencoded({extended: true}))
app.use(cookieParser())

routeIndex.route(app)
routeChat.route(app)
routeEnvoyerMessage.route(app)
routeLogin.route(app)
routeLogout.route(app)
routeAdmin.route(app)
routeChannels.route(app)

app.listen(port, () => {})