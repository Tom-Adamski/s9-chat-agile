function route(app)
{
	app.get('/logout', (req, res) =>
	{
        res.clearCookie('user')

		res.redirect('/login')
	})
}

module.exports =
{
	route: route
}