@echo off
cd %~dp0
title %cd%
color e
echo MEMO COMMANDES :
echo - (RE)CREER SCHEMA : .read creer_schema.sql
echo - INSERER DONNEES  : .read inserer_donnees.sql
echo.
sqlite3.exe  -cmd ".open ./database.db" -cmd "PRAGMA foreign_keys = 1"