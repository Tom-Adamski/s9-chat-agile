let facade = require('./facade')

function route(app)
{
	app.get('/admin', (req, res) =>
	{
        res.render('admin', {title: 'Administration'})
	})

	app.post('/admin/add_user', (req, res) =>
	{
		let parametres = req.body
		let email = parametres.email
		let pseudo = parametres.nom_complet
		let password = parametres.password


		facade.addUser(email, pseudo, password, (error, rows) =>{})

        res.redirect('/admin')
	})

	app.post('/admin/add_channel', (req, res) =>
	{
		let parametres = req.body
		let nomCanal = parametres.nom_canal

		facade.addChannel(nomCanal, (error, rows) =>{})

        res.redirect('/admin')
	})

}

module.exports =
{
	route: route
}