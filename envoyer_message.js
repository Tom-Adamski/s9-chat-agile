let facade = require('./facade')

function route(app)
{
	app.post('/envoyer_message/:id_channel', (req, res) =>
	{
        let parametres = req.body
        let idChannel = req.params.id_channel

        let content = parametres.message

        let email = req.cookies['user'].email
        
        if(content != undefined && content != "")
        {
            facade.addMessage(email, content, idChannel, (error) =>{})
        }	

		res.redirect(`/chat/${idChannel}`)
	})
}

module.exports =
{
	route: route
}