let facade = require('./facade')

function route(app)
{
	app.get('/', (req, res) =>
	{
		let user = req.cookies['user']
		if(user){
			res.redirect('/channels')
		}
		else{
			res.redirect('/login')
		}
	})
}

module.exports =
{
	route: route
}