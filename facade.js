// CONSTANTS /////////////////////////////////////////////////////////////////

const DBPATH = './database.db'
const sha1 = require('sha1')
const salt = '53cur17y'

// Driver pour base de données
const sqlite = require('sqlite3').verbose()
// Instance de ce driver
let bdd = new sqlite.Database(DBPATH, (error) =>
	{
		if (error)
		{
			console.error('KO : "' + DBPATH + '" :')
			console.error(error)
			throw error
		}
		else
		{
			bdd.exec('PRAGMA foreign_keys = ON')
			console.log('OK : "' + DBPATH + '" ok.')
		}
	}
)

function addMessage(email, content, id_channel,callback)
{
    let date = new Date().toISOString()
	bdd.run
	(
		'INSERT OR REPLACE INTO message (email_user, content, date, id_channel) VALUES (?, ?, ?, ?)',
		[email, content, date, id_channel],
		(error) =>
		{
			if (callback)
			{
				if (error)
				{
					callback(error, null)
				}
				else
				{
					callback(null, "OK")
				}
			}
		}
	)
}

function getMessagesFromChannel(id_channel, callback)
{
	bdd.all('SELECT u.pseudo, m.* FROM message m, user u WHERE id_channel = ? AND u.email = m.email_user ORDER BY date',
		[id_channel],
		(error, rows) =>
		{
			callback(error, rows)
		}
	);
}

function addUser(email, pseudo, password, callback)
{
	password = sha1(salt + password)

	bdd.run
	(
		'INSERT OR REPLACE INTO user (email, pseudo, password) VALUES (?,?,?)',
		[email, pseudo, password],
		(error) =>
		{
			if (callback)
			{
				if (error)
				{
					callback(error, null)
				}
				else
				{
					callback(null, "OK")
				}
			}
		}
	)
}

function getUser(email, password, callback)
{
	password = sha1(salt + password)

	bdd.all('SELECT pseudo, email FROM user WHERE email = ? AND password = ?',
		[email, password],
		(error, rows) =>
		{
			callback(error, rows)
		}
	);
}

function addChannel(name, callback)
{
	bdd.run
	(
		'INSERT OR REPLACE INTO channel (name) VALUES (?)',
		[name],
		(error) =>
		{
			if (callback)
			{
				if (error)
				{
					callback(error, null)
				}
				else
				{
					callback(null, "OK")
				}
			}
		}
	)
}

function getChannels(callback)
{
	bdd.all('SELECT * FROM channel ORDER BY name',
		(error, rows) =>
		{
			callback(error, rows)
		}
	);
}

// EXPORTS ////////////////////////////////////////////////////////////////////

module.exports = 
{
	addMessage : addMessage,
	getMessagesFromChannel : getMessagesFromChannel,
	getUser: getUser,
	getChannels: getChannels,
	addUser: addUser,
	addChannel: addChannel
}