let facade = require('./facade')

function route(app)
{
	app.get('/channels', (req, res) =>
	{
		let user = req.cookies['user']
		if(user){
			facade.getChannels((erreur, channels) =>
			{
				res.render('channels', { title: 'Channels', channels : channels, pseudo: user.pseudo })
			})
		}
		else{
			res.redirect('/login')
		}
		
	})
}

module.exports =
{
	route: route
}